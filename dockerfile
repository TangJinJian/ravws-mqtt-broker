FROM node:12.14.0

COPY . /root

EXPOSE 1883

CMD ["/root/wait-for-it.sh", "$MONGO_HOST:$MONGO_PORT", "--", "node", "/root/dist/main.js"]