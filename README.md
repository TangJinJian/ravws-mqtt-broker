# `ravws-mqtt-broker`

#### 一、介绍
远程自动洗车系统-`MQTT`-代理

```
MQTT URL: broker.ravws.dloa.xyz
```

#### 二、`Docker`部署
```sh
sh exec-docker-compose.sh
```

#### 三、`MQTT`主题

##### 1、洗车点状态

###### `topic`

```
ravws/carwashes/<洗车点编号>/status/patch
```

###### `QoS`

```
2
```

###### `payload`

```json
{
  "state": 0
}
```

| 属性    | 含义                                          |
| ------- | --------------------------------------------- |
| `state` | 洗车点的状态。0：空闲中、1：洗车中、2：维护中 |

##### 2、洗车点新订单

###### `topic`

```
ravws/orders/<洗车点编号>/post
```

###### `QoS`

```
2
```

###### `payload`

```json
{
  "orderNumber": "5e807401ab4b0d2603d50ccb",
  "plateNumber": "苏A12345",
  "appointmentTime": "2020-04-05 20:14:29",
  "vehicle": 0
}
```

| 属性              | 含义                                      |
| ----------------- | ----------------------------------------- |
| `orderNumber`     | 订单号                                    |
| `plateNumber`     | 车牌号                                    |
| `appointmentTime` | 预约时间                                  |
| `vehicle`         | 车型。0：小型车、1：微型车、2：紧凑车型…… |

##### 3、完成一个订单

###### `topic`

```
ravws/orders/complete/patch
```

###### `QoS`

```
2
```

###### `payload`

```json
{
  "orderNumber": "5e807401ab4b0d2603d50ccb"
}
```

| 属性          | 含义   |
| ------------- | ------ |
| `orderNumber` | 订单号 |

##### 4、洗车点警告信息

###### `topic`

```
ravws/warnings/<洗车点编号>/post
```

###### `QoS`

```
2
```

###### `payload`

```json
{
  "errorCode": 0,
  "message": "洗车液不足"
}
```

| 属性        | 含义                                              |
| ----------- | ------------------------------------------------- |
| `errorCode` | 错误代码。0：洗车液不足、1：电机故障、2：履带故障 |
| `message`   | 警告信息                                          |

##### 5、撤销一个订单

###### `topic`

```
ravws/orders/undo/patch
```

###### `QoS`

```
2
```

###### `payload`

```json
{
  "orderNumber": "5e807401ab4b0d2603d50ccb"
}
```
