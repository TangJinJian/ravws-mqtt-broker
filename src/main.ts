import { Server, ServerOptions } from 'mosca';
import { generatedFromEnvMongoURL } from './mongo-env-url';

// 定义听众
const ascoltatore = {
  type: 'mongo',
  url: generatedFromEnvMongoURL(),
  pubsubCollection: process.env.PUBSUB_COLLECTION,
  mongo: {},
};

// 定义MQTT配置
const setting: ServerOptions = {
  port: 1883,
  backend: ascoltatore,
};

const server = new Server(setting);

server.on('ready', () => console.log('ravws-mqtt-broker 服务器已经启动并运行'));
