/**
 * 从环境变量生成 MongoDB 连接 URL
 */
export const generatedFromEnvMongoURL = (): string => {
  const {
    // 主机
    MONGO_HOST,
    // 端口
    MONGO_PORT,
    // 数据库
    MONGO_DB,
    // 认证用户名
    MONGO_INITDB_ROOT_USERNAME,
    // 认证密码
    MONGO_INITDB_ROOT_PASSWORD,
    // 认证源
    AUTH_SOURCE,
  } = process.env;

  let mongoURL = `${MONGO_HOST}:${MONGO_PORT}/${MONGO_DB}`;

  // 如果传入了用户认证
  if (MONGO_INITDB_ROOT_USERNAME !== undefined) {
    mongoURL = `${MONGO_INITDB_ROOT_USERNAME}:${MONGO_INITDB_ROOT_PASSWORD}@${mongoURL}?authSource=${AUTH_SOURCE}`;
  }

  return `mongodb://${mongoURL}`;
};
